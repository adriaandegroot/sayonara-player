/* LyricsPreferences.cpp, (Created on 14.05.2024) */

/* Copyright (C) 2011-2024 Michael Lugmair
 *
 * This file is part of Sayonara Player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GUI_LyricsPreferences.h"
#include "Gui/Preferences/ui_GUI_LyricsPreferences.h"

#include "Components/Lyrics/LyricsServerProvider.h"
#include "Components/Lyrics/LyricServer.h"

#include "Utils/Language/Language.h"
#include "Utils/LyricServerEntry.h"
#include "Utils/Settings/Settings.h"

#include <QList>

GUI_LyricsPreferences::GUI_LyricsPreferences(const QString& identifier) :
	Preferences::Base(identifier) {}

GUI_LyricsPreferences::~GUI_LyricsPreferences() = default;

bool GUI_LyricsPreferences::commit()
{
	auto serverEntries = QList<Lyrics::ServerEntry> {};
	const auto items = ui->listView->items();
	for(const auto& item: items)
	{
		serverEntries << Lyrics::ServerEntry(item.text, item.checked);
	}

	SetSetting(Set::Lyrics_ServerEntries, serverEntries);

	return true;
}

void GUI_LyricsPreferences::revert()
{
	ui->listView->clear();

	const auto map = Lyrics::getActiveEntryMap();
	const auto servers = Lyrics::getSortedServerList(false, Lyrics::defaultLyricConfiguration());
	for(auto* server: servers)
	{
		const auto name = server->name();
		const auto isDisabled = map.contains(name) && (map[name] == false); // NOLINT(*-simplify-boolean-expr)
		ui->listView->append(Gui::CheckableItem {!isDisabled, name});
	}
}

QString GUI_LyricsPreferences::actionName() const { return Lang::get(Lang::Lyrics); }

void GUI_LyricsPreferences::initUi()
{
	ui = std::make_shared<Ui::GUI_LyricsPreferences>();
	ui->setupUi(this);
	ui->listView->init();
}

void GUI_LyricsPreferences::retranslate()
{
	ui->labLyricsProvider->setText(Lang::get(Lang::LyricsProvider));
}
