/* LyricServer.h */

/* Copyright (C) 2012 Michael Lugmair (Lucio Carreras)
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LYRICSERVER_H_
#define LYRICSERVER_H_

#include <QString>
#include <QList>
#include <QPair>
#include <QMap>
#include "Utils/Pimpl.h"

class QJsonObject;

namespace Lyrics
{
	class Server
	{
		PIMPL(Server)

		public:
			Server();
			~Server();

			using StartEndTag = QPair<QString, QString>;
			using StartEndTags = QList<StartEndTag>;
			using Replacement = QPair<QString, QString>;
			using Replacements = QList<Replacement>;

			[[nodiscard]] bool canFetchDirectly() const;
			[[nodiscard]] bool canSearch() const;

			[[nodiscard]] QString name() const;
			void setName(const QString& name);

			[[nodiscard]] QString address() const;
			void setAddress(const QString& address);

			[[nodiscard]] Replacements replacements() const;
			void setReplacements(const Replacements& replacements);

			[[nodiscard]] QString directUrlTemplate() const;
			void setDirectUrlTemplate(const QString& directUrlTemplate);

			[[nodiscard]] StartEndTags startEndTag() const;
			void setStartEndTag(const StartEndTags& startEndTag);

			[[nodiscard]] bool isStartTagIncluded() const;
			void setIsStartTagIncluded(bool isStartTagIncluded);

			[[nodiscard]] bool isEndTagIncluded() const;
			void setIsEndTagIncluded(bool isEndTagIncluded);

			[[nodiscard]] bool isNumeric() const;
			void setIsNumeric(bool isNumeric);

			[[nodiscard]] bool isLowercase() const;
			void setIsLowercase(bool isLowercase);

			[[nodiscard]] QString errorString() const;
			void setErrorString(const QString& errorString);

			[[nodiscard]] QString searchResultRegex() const;
			void setSearchResultRegex(const QString& searchResultRegex);

			[[nodiscard]] QString searchResultUrlTemplate() const;
			void setSearchResultUrlTemplate(const QString& searchResultUrlTemplate);

			[[nodiscard]] QString searchUrlTemplate() const;
			void setSearchUrlTemplate(const QString& searchUrlTemplate);

			QJsonObject toJson();
			static ::Lyrics::Server* fromJson(const QJsonObject& json);

			static QString applyReplacements(const QString& str, const Server::Replacements& replacements);

		private:
			[[nodiscard]] QString applyReplacements(const QString& str) const;
	};
}

#endif /* LYRICSERVER_H_ */
